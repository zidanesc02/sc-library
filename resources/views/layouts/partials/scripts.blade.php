<!-- Vendor JS -->
<script src="{{ asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/libs/metismenujs/metismenujs.min.js') }}"></script>
<script src="{{ asset('assets/libs/simplebar/simplebar.min.js') }}"></script>
<script src="{{ asset('assets/libs/feather-icons/feather.min.js') }}"></script>

<!-- gridjs js -->
<script src="{{ asset('assets/libs/gridjs/gridjs.umd.js') }}"></script>

<!-- Sweet Alerts js -->
<script src="{{ asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>

<!-- apexcharts -->
<script src="{{ asset('assets/libs/apexcharts/apexcharts.min.js') }}"></script>
<!-- Chart JS -->
<script src="{{ asset('assets/js/pages/chartjs.js') }}"></script>s

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

<script src="{{ asset('assets/js/app.js') }}"></script>

<!-- jQuery CDN -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- select2 -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<!-- Datatables JS CDN -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>



<script>
    $('.select2').select2();

    @if (session()->has('success'))
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: "{{ session()->get('success') }}",
            showConfirmButton: true,
            timer: 1500
        })
    @endif

    @if (session()->has('failed'))
        Swal.fire({
            position: 'center',
            icon: 'error',
            title: "{{ session()->get('failed') }}",
            showConfirmButton: true,
            timer: 3000
        })
    @endif
    // Logout function
    function logout() {
        Swal.fire({
            imageUrl: "{{ asset('assets/images/gif/logout.gif') }}",
            title: 'Logout',
            html: 'Are you sure want to logout?',
            confirmButtonText: 'Yes',
            timerProgressBar: true,
            showCancelButton: true,
            timer: 5000,
        }).then(function(result) {
            if (result.isConfirmed) {
                axios({
                    method: 'post',
                    url: "{{ route('logout') }}",
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    data: {
                        "_token": "{{ csrf_token() }}"
                    }
                }).then((response) => {
                    location.reload();
                }).catch((err) => {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Logout Failed!!',
                    })
                })
            }
        });
    }

    // Delete Function
    function modalDelete(title, name, url, redirect_link) {
        Swal.fire({
            title: `Delete ${title}?`,
            html: `Are you sure want to delete ${name}`,
            timer: '8000',
            confirmButtonText: 'Delete',
            timerProgressBar: true,
            showCancelButton: true,
            cancelButtonText: 'Cancel',
        }).then(function(result) {
            if (result.isConfirmed) {
                axios({
                    method: 'post',
                    url: url,
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    data: {
                        "_method": "DELETE",
                        "_token": "{{ csrf_token() }}"
                    }
                }).then((response) => {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: response.data.message,
                        showConfirmButton: true,
                        timer: 1500
                    }).then(() => {
                        window.location.href = redirect_link
                    })
                }).catch((err) => {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Failed to delete!!',
                    })
                })
            }
        });
    }

    // Detail modal
    // function detailModal(title, url, width) {
    //     $.confirm({
    //         title: title,
    //         theme: 'material',
    //         backgroundDismiss: true, // this will just close the modal
    //         content: 'URL:' + url,
    //         animation: 'zoom',
    //         closeAnimation: 'scale',
    //         animationSpeed: 400,
    //         closeIcon: true,
    //         columnClass: width,
    //         buttons: {
    //             close: {
    //                 btnClass: 'btn-dark font-bold',
    //             }
    //         },
    //     });
    // }
</script>
@stack('scripts')
