@extends('layouts.app')

@section('content')
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Books</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Books</li>
                </ol>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">All Books</h4>
                    <div class="card-header-action">
                        <a href="{{ route('books.create') }}" class="btn btn-primary btn-sm">
                            <i class="mdi mdi-plus"></i> Create
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table mb-0" id="bookTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Cover</th>
                                    <th>ISBN</th>
                                    <th>Title</th>
                                    <th>Category</th>
                                    <th>Shelf</th>
                                    <th>Stock</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end card -->
        </div>
        <!-- end col -->
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        function addStock(bookId, bookName) {
            Swal.fire({
                title: "Add book stock " + bookName,
                html: '<label for="stock-add">Stock</label>' +
                    '<input id="stock-add" type="number" value="0" class="swal2-input">' +
                    '<textarea id="description" class="swal2-textarea" placeholder="description"></textarea>',
                showCancelButton: !0,
                confirmButtonText: "Submit",
                showLoaderOnConfirm: !0,
                confirmButtonColor: "#3980c0",
                cancelButtonColor: "#f34e4e",
            }).then(function(result) {
                if (result.isConfirmed) {
                    const stock = $('#stock-add').val()
                    const description = $('#description').val()
                    axios({
                        method: 'post',
                        url: "/api/books/add-stock/" + bookId,
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        },
                        data: {
                            "_token": "{{ csrf_token() }}",
                            stock,
                            description
                        }
                    }).then((response) => {
                        Swal.fire({
                            icon: 'success',
                            title: "Success",
                            text: "Stock added",
                            showConfirmButton: true,
                            timer: 1500
                        }).then(() => {
                            location.reload()
                        })
                    }).catch((err) => {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: err.response.data.message,
                        })
                    })
                }
            })
        }

        $(document).ready(function() {
            // DataTable
            $('#bookTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('books.json') }}",
                columns: [{
                        name: 'no',
                        data: 'id',
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                        orderable: false,
                    },
                    {
                        name: 'cover',
                        data: 'cover',
                        orderable: false
                    },
                    {
                        name: 'isbn',
                        data: 'isbn',
                        orderable: false
                    },
                    {
                        name: 'title',
                        data: 'title',
                        orderable: true,
                    },
                    {
                        name: 'category',
                        data: 'category',
                        orderable: false,
                    },
                    {
                        name: 'shelf',
                        data: 'shelf',
                        orderable: false,
                    },
                    {
                        name: 'stock',
                        data: 'stock',
                        orderable: true,
                    },
                    {
                        name: 'action',
                        data: 'action',
                        orderable: false
                    },
                ]
            });

        });
    </script>
@endpush
