@extends('layouts.app')

@section('content')
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Book</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active">Detail</li>
                </ol>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-4">
                        <div class="product-detail">
                            <div class="tab-content position-relative border" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="product-1" role="tabpanel">
                                    <div class="product-img">
                                        <img src="{{ env('APP_URL') . '/storage//' . ($book->cover ? $book->cover : 'no-cover.jpg') }}" alt="" class="img-fluid mx-auto d-block" data-zoom="assets/images/product/img-1.png">
                                    </div>
                                </div>
                            </div>

                            <div class="row text-center mt-2">
                                <div class="col-sm-6">
                                    <div class="d-grid">
                                        <a href="{{ route('cart.add', $book->id) }}" class="btn btn-primary waves-effect waves-light mt-2 me-1">
                                            <i class="bx bx-cart-alt me-2"></i> Add to cart
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8">
                        <div class="mt-4 mt-xl-3 ps-xl-4">
                            <h5 class="font-size-14"><a href="#" class="text-muted">Title</a></h5>
                            <h4 class="font-size-20 mb-3">{{ $book->title }}</h4>

                            <div class="text-muted">
                                <span class="badge bg-warning font-size-14 me-1"><i class="mdi mdi-star"></i> 4.2</span> 234 Reviews
                            </div>

                            <h5 class="mt-4 pt-2">{{ $book->stock }} Left<span class="text-danger font-size-14 ms-2"></span><span class="badge {{ ($book->stock > 0) ? 'bg-success' : 'bg-danger' }} font-size-14 me-1">{{ ($book->stock > 0) ? 'Available' : 'Not Available' }}</span></h5>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                <div class="mt-4 pt-3">
                    <h5 class="font-size-14 mb-3">Book Detail: </h5>
                    <div class="product-desc">
                        <ul class="nav nav-tabs nav-tabs-custom" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" id="desc-tab" data-bs-toggle="tab" href="#desc" role="tab">Synopsis</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" id="specifi-tab" data-bs-toggle="tab" href="#specifi" role="tab">Specifications</a>
                            </li>
                        </ul>
                        <div class="tab-content border border-top-0 p-4">
                            <div class="tab-pane fade" id="desc" role="tabpanel">
                                <div class="row">
                                    <div class="col-sm-9 col-md-10">
                                        <div class="text-muted p-2">
                                            {{ $book->synopsis }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade show active" id="specifi" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="table table-nowrap mb-0">
                                        <tbody>
                                            <tr>
                                                <th scope="row">ISBN</th>
                                                <td>{{ $book->isbn }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row" style="width: 20%;">Category</th>
                                                <td>{{ $book->category }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Shelf</th>
                                                <td>{{ $book->shelf }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Published Year</th>
                                                <td>{{ $book->published_year }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Author</th>
                                                <td>{{ $book->author }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Publisher</th>
                                                <td>{{ $book->publisher }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Page Count</th>
                                                <td>{{ $book->page_count }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Stock</th>
                                                <td>{{ $book->stock }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt-4 pt-3">
                    <h5 class="font-size-14 mb-3">Reviews : </h5>
                    <div class="text-muted mb-3">
                        <span class="badge bg-success font-size-14 me-1"><i class="mdi mdi-star"></i> 4.2</span> 234 Reviews
                    </div>
                    <div class="border p-4 rounded">
                        <div class="border-bottom pb-3">
                            <p class="float-sm-end text-muted font-size-13">12 July, 2020</p>
                            <div class="badge bg-success mb-2"><i class="mdi mdi-star"></i> 4.1</div>
                            <p class="text-muted mb-4">Cool book</p>
                            <div class="d-flex align-items-start">
                                <div class="flex-grow-1">
                                    <h5 class="font-size-15 mb-0">Samuel</h5>
                                </div>

                                <div class="flex-shrink-0">
                                    <ul class="list-inline product-review-link mb-0">
                                        <li class="list-inline-item">
                                            <a href="#"><i class="bx bx-like"></i></a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="#"><i class="bx bx-comment-dots"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>

                        <div class="border-bottom py-3">
                            <p class="float-sm-end text-muted font-size-13">06 July, 2020</p>
                            <div class="badge bg-success mb-2"><i class="mdi mdi-star"></i> 4.0</div>
                            <p class="text-muted mb-4">This book is very recommend for you guys</p>
                            <div class="d-flex align-items-start">
                                <div class="flex-grow-1">
                                    <h5 class="font-size-15 mb-0">Joseph</h5>
                                </div>

                                <div class="flex-shrink-0">
                                    <ul class="list-inline product-review-link mb-0">
                                        <li class="list-inline-item">
                                            <a href="#"><i class="bx bx-like"></i></a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="#"><i class="bx bx-comment-dots"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="border-bottom py-3">
                            <p class="float-sm-end text-muted font-size-13">26 June, 2020</p>
                            <div class="badge bg-success mb-2"><i class="mdi mdi-star"></i> 4.2</div>
                            <p class="text-muted mb-4">Insightful</p>
                            <div class="d-flex align-items-start">
                                <div class="flex-grow-1">
                                    <h5 class="font-size-15 mb-0">Paul</h5>
                                </div>

                                <div class="flex-shrink-0">
                                    <ul class="list-inline product-review-link mb-0">
                                        <li class="list-inline-item">
                                            <a href="#"><i class="bx bx-like"></i></a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="#"><i class="bx bx-comment-dots"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
