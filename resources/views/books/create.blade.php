@extends('layouts.app')

@section('content')
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Books</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('books.index') }}">Books</a></li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>
            </div>

        </div>
    </div>

    <div class="card">
        <div class="p-4 border-top">
            <form method="POST" action="{{ route('books.store') }}" enctype="multipart/form-data">
                @csrf

                <div class="row">
                    <div class="col-sm-3">
                        <div class="mb-3">
                            <label class="form-label" for="isbn">ISBN<span class="fw-bold text-danger">*</strong></label>
                            <input id="isbn" name="isbn" type="text" placeholder="Enter ISBN" class="form-control @error('isbn') is-invalid @enderror" value="{{ old('isbn') }}">
                            @error('isbn')
                                <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="mb-3">
                            <label class="form-label" for="title">Title<span class="fw-bold text-danger">*</strong></label>
                            <input id="title" name="title" placeholder="Enter title" type="text" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') }}">
                            @error('title')
                                <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <label for="category" class="form-label font-size-13 text-muted">Category<span class="fw-bold text-danger">*</strong></label>
                        <select class="form-control select2 @error('category') is-invalid @enderror" name="category" id="category">
                            <option value="">Choose Category</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}" {{ old('category') == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                            @endforeach
                        </select>
                        @error('category')
                            <span class="invalid-feedback">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-sm-3">
                        <div class="mb-3">
                            <label for="shelf" class="form-label font-size-13 text-muted">Shelf<span class="fw-bold text-danger">*</strong></label>
                            <select class="form-control select2 @error('shelf') is-invalid @enderror" name="shelf" id="shelf">
                                <option value="">Choose Shelf</option>
                                @foreach ($shelves as $shelf)
                                    <option value="{{ $shelf->id }}" {{ old('shelf') == $shelf->id ? 'selected' : '' }}>{{ $shelf->code }}</option>
                                @endforeach
                            </select>
                            @error('shelf')
                                <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <div class="mb-3">
                            <label class="form-label" for="published_year">Published Year<span class="fw-bold text-danger">*</strong></label>
                            <input id="published_year" name="published_year" type="number" placeholder="Published Year" class="form-control @error('published_year') is-invalid @enderror" value="{{ old('published_year') }}">
                            @error('published_year')
                                <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="mb-3">
                            <label class="form-label" for="stock">Stock<span class="fw-bold text-danger">*</strong></label>
                            <input id="stock" name="stock" placeholder="Enter Stock" type="number" class="form-control @error('stock') is-invalid @enderror" value="{{ old('stock') }}">
                            @error('stock')
                                <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="mb-3">
                            <label class="form-label" for="author">Author<span class="fw-bold text-danger">*</strong></label>
                            <input id="author" name="author" placeholder="Enter Author" type="text" class="form-control @error('author') is-invalid @enderror" value="{{ old('author') }}">
                            @error('author')
                                <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="mb-3">
                            <label class="form-label" for="publisher">Publisher<span class="fw-bold text-danger">*</strong></label>
                            <input id="publisher" name="publisher" placeholder="Enter Publisher" type="text" class="form-control @error('publisher') is-invalid @enderror" value="{{ old('publisher') }}">
                            @error('publisher')
                                <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="mb-3">
                            <label class="form-label" for="page_count">Page Count<span class="fw-bold text-danger">*</strong></label>
                            <input id="page_count" name="page_count" placeholder="Enter Page Count" type="number" class="form-control @error('page_count') is-invalid @enderror" value="{{ old('page_count') }}">
                            @error('page_count')
                                <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="mb-3">
                    <label class="form-label" for="cover">Cover</label>
                    <input type="file" class="form-control @error('cover') is-invalid @enderror" id="cover" name="cover" value="{{ old('cover') }}">
                    @error('cover')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="mb-0">
                    <label class="form-label" for="synopsis">Synopsis<span class="fw-bold text-danger">*</strong></label>
                    <textarea class="form-control @error('synopsis') is-invalid @enderror" id="synopsis" placeholder="Enter Synopsis" rows="4" name="synopsis">{{ old('synopsis') }}</textarea>
                    @error('synopsis')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="row mt-3">
                    <div class="col text-end">
                        <a href="{{ route('books.index') }}" class="btn btn-danger"> <i class="bx bx-x me-1"></i> Cancel </a>
                        <button type="submit" class="btn btn-success"> <i class=" bx bx-file me-1"></i> Save </a>
                    </div> <!-- end col -->
                </div> <!-- end row-->
            </form>
        </div>
    </div>
@endsection

@push('scripts')
@endpush
