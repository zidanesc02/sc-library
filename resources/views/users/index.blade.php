@extends('layouts.app')

@section('content')
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Users</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Users</li>
                </ol>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header align-items-center d-flex">
                    <div class="mb-0 flex-grow-1">
                        <a href="{{ route('users.create') }}" class="btn btn-success waves-effect waves-light"><i class="mdi mdi-plus me-2"></i> Add New</a>
                    </div>
                    <div class="flex-shrink-0">
                        <ul class="nav justify-content-end nav-pills card-header-pills" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-bs-toggle="tab" href="#request" role="tab">
                                    <span class="d-none d-sm-block">Request</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-bs-toggle="tab" href="#member" role="tab">
                                    <span class="d-none d-sm-block">Member</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-bs-toggle="tab" href="#admin" role="tab">
                                    <span class="d-none d-sm-block">Admin</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div><!-- end card header -->

                <div class="card-body">

                    <!-- Tab panes -->
                    <div class="tab-content text-muted">
                        <div class="tab-pane active" id="request" role="tabpanel">
                            <!-- end row -->
                            <div class="table-responsive mb-4">
                                <table class="table table-centered table-nowrap mb-0" id="requestTable" width="100%">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Active</th>
                                            <th scope="col" style="width: 200px;">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="member" role="tabpanel">
                            <div class="table-responsive mb-4">
                                <table class="table table-centered table-nowrap mb-0" id="memberTable" width="100%">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Active</th>
                                            <th scope="col" style="width: 200px;">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="admin" role="tabpanel">
                            <div class="table-responsive mb-4">
                                <table class="table table-centered table-nowrap mb-0" id="adminTable" width="100%">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Active</th>
                                            <th scope="col" style="width: 200px;">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end card-body -->
            </div><!-- end card -->
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        function changeStatus(status, id) {
            axios({
                method: 'post',
                url: "/admin/users/change-status/" + id + "/" + status,
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                data: {
                    "_token": "{{ csrf_token() }}",
                }
            }).then((response) => {
                Swal.fire({
                    icon: 'success',
                    title: "Success",
                    text: "Change status success",
                    showConfirmButton: true,
                    timer: 1500
                }).then(() => {
                    location.reload()
                })
            }).catch((err) => {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: err,
                })
            })
        }

        $(document).ready(function() {

            // DataTable
            $('#requestTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('users.request.json') }}",
                columns: [{
                        name: 'no',
                        data: 'id',
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                        orderable: false,
                    },
                    {
                        name: 'name',
                        data: 'name',
                        orderable: true
                    },
                    {
                        name: 'email',
                        data: 'email',
                        orderable: true,
                    },
                    {
                        name: 'status',
                        data: 'status',
                        orderable: false,
                    },
                    {
                        name: 'action',
                        data: 'action',
                        orderable: false
                    },
                ]
            });

            $('#memberTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('users.member.json') }}",
                columns: [{
                        name: 'no',
                        data: 'id',
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                        orderable: false,
                    },
                    {
                        name: 'name',
                        data: 'name',
                        orderable: true
                    },
                    {
                        name: 'email',
                        data: 'email',
                        orderable: true,
                    },
                    {
                        name: 'status',
                        data: 'status',
                        orderable: false,
                    },
                    {
                        name: 'action',
                        data: 'action',
                        orderable: false
                    },
                ]
            });

            $('#adminTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('users.admin.json') }}",
                columns: [{
                        name: 'no',
                        data: 'id',
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                        orderable: false,
                    },
                    {
                        name: 'name',
                        data: 'name',
                        orderable: true
                    },
                    {
                        name: 'email',
                        data: 'email',
                        orderable: true,
                    },
                    {
                        name: 'status',
                        data: 'status',
                        orderable: false,
                    },
                    {
                        name: 'action',
                        data: 'action',
                        orderable: false
                    },
                ]
            });

        });
    </script>
@endpush
