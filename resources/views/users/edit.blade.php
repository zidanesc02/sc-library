@extends('layouts.app')

@section('content')
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Users</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('users.index') }}">Users</a></li>
                    <li class="breadcrumb-item active">Edit</li>
                </ol>
            </div>

        </div>
    </div>

    <div class="card">
        <div class="p-4 border-top">
            <form method="POST" action="{{ route('users.update', $user->id) }}" enctype="multipart/form-data">
                @method('PATCH')
                @csrf

                <div class="mb-3">
                    <label class="form-label" for="name">Name</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Enter Name" name="name" value="{{ old('name') ?? $user->name }}">
                    @error('name')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <label class="form-label" for="email">Email</label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Enter email" name="email" value="{{ old('email') ?? $user->email }}">
                    @error('email')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <label class="form-label" for="phone_number">Phone Number</label>
                    <input class="form-control @error('phone_number') is-invalid @enderror" type="tel" placeholder="Enter Phone Number" id="phone_number" name="phone_number" value="{{ old('phone_number') ?? $user->phone_number }}">
                    @error('phone_number')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <label class="form-label" for="avatar">Avatar</label>
                    <img src="{{ env('APP_URL') . '/storage//' . ($user->avatar ? $user->avatar : 'no-cover.jpg') }}" alt="{{ $user->name }}" class="img-thumbnail" style="width: 200px;">
                    @if ($user->avatar)
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="checkbox" id="avatar_default" name="avatar_default" class="custom-control-input" value="true" {{ old('avatar_default') == true ? 'checked' : '' }}>
                            <label class="custom-control-label" for="avatar_default">Remove</label>
                        </div>
                    @endif
                    <input type="file" class="form-control @error('avatar') is-invalid @enderror" id="avatar" name="avatar" value="{{ old('avatar') }}">
                    @error('avatar')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <label class="form-label" for="address">Address</label>
                    <textarea class="form-control @error('address') is-invalid @enderror" id="address" name="address" rows="3" name="address" placeholder="Enter Address">{{ old('address') ?? $user->address }}</textarea>
                    @error('address')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <label class="form-label" for="gender">Gender</label>
                    <div class="form-check mb-3">
                        <input class="form-check-input" type="radio" name="gender" id="male" value="male" checked {{ (old('gender') ?? $user->gender) == 'male' ? 'checked' : '' }}>
                        <label class="form-check-label" for="male">
                            Male
                        </label>
                    </div>
                    <div class="form-check mb-3">
                        <input class="form-check-input" type="radio" name="gender" id="female" value="female" {{ (old('gender') ?? $user->gender) == 'female' ? 'checked' : '' }}>
                        <label class="form-check-label" for="female">
                            Female
                        </label>
                    </div>
                </div>

                <div class="mb-3">
                    <label class="form-label" for="role">Role</label>
                    <div class="form-check mb-3">
                        <input class="form-check-input" type="radio" name="role" id="admin" value="admin" checked {{ (old('role') ?? $user->role) == 'admin' ? 'checked' : '' }}>
                        <label class="form-check-label" for="admin">
                            Admin
                        </label>
                    </div>
                    <div class="form-check mb-3">
                        <input class="form-check-input" type="radio" name="role" id="member" value="member" {{ (old('role') ?? $user->role) == 'member' ? 'checked' : '' }}>
                        <label class="form-check-label" for="member">
                            Member
                        </label>
                    </div>
                </div>

                <div class="mb-3">
                    <label class="form-label" for="new_password">New Password</label>
                    <input type="password" class="form-control @error('new_password') is-invalid @enderror" id="new_password" placeholder="Enter new password" name="new_password">
                    @error('new_password')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="row mt-3">
                    <div class="col text-end">
                        <a href="{{ route('users.index') }}" class="btn btn-danger"> <i class="bx bx-x me-1"></i> Cancel </a>
                        <button type="submit" class="btn btn-success"> <i class=" bx bx-file me-1"></i> Save </a>
                    </div> <!-- end col -->
                </div> <!-- end row-->
            </form>
        </div>
    </div>
@endsection

@push('scripts')
@endpush
