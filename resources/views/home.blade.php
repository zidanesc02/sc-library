@extends('layouts.app')

@section('content')
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Home</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="">Library</a></li>
                    <li class="breadcrumb-item active">Home</li>
                </ol>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-xl-3 col-lg-4">
            <div class="card">
                <div class="card-header bg-transparent border-bottom">
                    <h5 class="mb-0">Filters</h5>
                </div>

                <div class="p-4 border-top">
                    <div>
                        <h5 class="font-size-14 mb-0"><a href="#filterproduct-category-collapse" class="collapsed text-dark d-block" data-bs-toggle="collapse">Categories <i class="mdi mdi-chevron-up float-end accor-down-icon"></i></a></h5>

                        <div class="collapse" id="filterproduct-category-collapse">
                            <div class="mt-4">
                                @foreach ($categories as $category)
                                    <div class="custom-control custom-radio mt-2">
                                        <input type="radio" id="{{ $category->name }}" name="category" class="custom-control-input">
                                        <label class="custom-control-label" for="{{ $category->name }}">{{ $category->name }}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>

                <div class="custom-accordion">
                    <div class="p-4 border-top">
                        <div>
                            <h5 class="font-size-14 mb-0"><a href="#filtershelf-collapse" class="text-dark d-block" data-bs-toggle="collapse">Shelf <i class="mdi mdi-chevron-up float-end accor-down-icon"></i></a></h5>

                            <div class="collapse show" id="filtershelf-collapse">
                                <div class="mt-4">
                                    @foreach ($shelves as $shelf)
                                        <div class="custom-control custom-radio mt-2">
                                            <input type="radio" id="{{ $shelf->code }}" name="shelf" class="custom-control-input">
                                            <label class="custom-control-label" for="{{ $shelf->code }}">{{ $shelf->code }}</label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="p-4 border-top">
                        <div>
                            <h5 class="font-size-14 mb-0"><a href="#filterproduct-color-collapse" class="text-dark d-block" data-bs-toggle="collapse">Customer Rating <i class="mdi mdi-chevron-up float-end accor-down-icon"></i></a></h5>

                            <div class="collapse show" id="filterproduct-color-collapse">
                                <div class="mt-4">
                                    <div class="form-check mt-2">
                                        <input type="radio" id="productratingRadio1" name="productratingRadio1" class="form-check-input">
                                        <label class="form-check-label" for="productratingRadio1">4 <i class="mdi mdi-star text-warning"></i> & Above</label>
                                    </div>
                                    <div class="form-check mt-2">
                                        <input type="radio" id="productratingRadio2" name="productratingRadio1" class="form-check-input">
                                        <label class="form-check-label" for="productratingRadio2">3 <i class="mdi mdi-star text-warning"></i> & Above</label>
                                    </div>
                                    <div class="form-check mt-2">
                                        <input type="radio" id="productratingRadio3" name="productratingRadio1" class="form-check-input">
                                        <label class="form-check-label" for="productratingRadio3">2 <i class="mdi mdi-star text-warning"></i> & Above</label>
                                    </div>
                                    <div class="form-check mt-2">
                                        <input type="radio" id="productratingRadio4" name="productratingRadio1" class="form-check-input">
                                        <label class="form-check-label" for="productratingRadio4">1 <i class="mdi mdi-star text-warning"></i></label>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div class="col-xl-9 col-lg-8">
            <div class="card">
                <div class="card-body">
                    <div>
                        <div class="row">
                            <div class="col-md-6">
                                <div>
                                    @if (Request::get('search'))
                                        <h5>Showing result for "{{ Request::get('search') }}"</h5>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-inline float-md-end">
                                    <div class="search-box ms-2">
                                        <div class="position-relative">
                                            <input type="text" class="form-control bg-light border-light rounded" placeholder="Search...">
                                            <i class="mdi mdi-magnify search-icon"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="row">
                            @foreach ($books as $book)
                                <div class="col-xl-4 col-sm-6" onclick="location.href='{{ route('book.show', $book->id) }}'">
                                    <div class="product-box">
                                        <div class="product-img pt-4 px-4">
                                            <div class="product-wishlist">
                                                <a href="{{ route('cart.add', $book->id) }}">
                                                    <i class="mdi mdi-cart-outline"></i>
                                                </a>
                                            </div>
                                            <img src="{{ env('APP_URL') . '/storage//' . ($book->cover ? $book->cover : 'no-cover.jpg') }}" alt="" class="img-fluid mx-auto d-block">
                                        </div>

                                        <div class="product-content p-4">

                                            <div class="d-flex justify-content-between align-items-end">
                                                <div>
                                                    <h5 class="mb-1"><a href="#" class="text-dark font-size-16">{{ $book->title }}</a></h5>
                                                    <p class="text-muted font-size-13">{{ $book->category . ' | ' . $book->shelf }}</p>
                                                    <h5 class="mt-3 mb-0"><span class="text-muted me-2">By {{ $book->author }}</h5>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                @endforeach
                        </div>
                        <!-- end row -->
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
