@extends('layouts.app')

@section('content')
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Transactions</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Transactions</li>
                </ol>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">All Transactions</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table mb-0" id="transactionTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Requested At</th>
                                    <th>Books</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end card -->
        </div>
        <!-- end col -->
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        function changeStatus(status, id) {
            axios({
                method: 'post',
                url: "/admin/transactions/change-status/" + id + "/" + status,
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                data: {
                    "_token": "{{ csrf_token() }}",
                }
            }).then((response) => {
                Swal.fire({
                    icon: 'success',
                    title: "Success",
                    text: "Change status success",
                    showConfirmButton: true,
                    timer: 1500
                }).then(() => {
                    location.reload()
                })
            }).catch((err) => {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: err,
                })
            })
        }

        $(document).ready(function() {

            // DataTable
            $('#transactionTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('transactions.json') }}",
                columns: [{
                        name: 'no',
                        data: 'id',
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                        orderable: false,
                    },
                    {
                        name: 'member_name',
                        data: 'member_name',
                        orderable: true
                    },
                    {
                        name: 'member_email',
                        data: 'member_email',
                        orderable: true,
                    },
                    {
                        name: 'requested_at',
                        data: 'requested_at',
                        orderable: true,
                    },
                    {
                        name: 'books',
                        data: 'books',
                        orderable: false,
                    },
                    {
                        name: 'status',
                        data: 'status',
                        orderable: false,
                    },
                ]
            });
        });
    </script>
@endpush
