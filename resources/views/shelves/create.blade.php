@extends('layouts.app')

@section('content')
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Shelves</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('shelves.index') }}">Shelves</a></li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>
            </div>

        </div>
    </div>

    <div class="card">
        <div class="p-4 border-top">
            <form method="POST" action="{{ route('shelves.store') }}">
                @csrf

                <div class="row">
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label class="form-label" for="code">Code<span class="fw-bold text-danger">*</strong></label>
                            <input id="code" name="code" type="text" class="form-control @error('code') is-invalid @enderror" readonly value="{{ $code }}">
                            @error('code')
                                <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label class="form-label" for="location">Location<span class="fw-bold text-danger">*</strong></label>
                            <input id="location" name="location" placeholder="Enter Location" type="text" class="form-control @error('location') is-invalid @enderror" value="{{ old('location') }}">
                            @error('location')
                                <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="mb-0">
                    <label class="form-label" for="metadescription">Description</label>
                    <textarea class="form-control" id="metadescription" placeholder="Enter Description" rows="4">{{ old('address') }}</textarea>
                    @error('description')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="row mt-3">
                    <div class="col text-end">
                        <a href="{{ route('shelves.index') }}" class="btn btn-danger"> <i class="bx bx-x me-1"></i> Cancel </a>
                        <button type="submit" class="btn btn-success"> <i class=" bx bx-file me-1"></i> Save </a>
                    </div> <!-- end col -->
                </div> <!-- end row-->
            </form>
        </div>
    </div>
@endsection

@push('scripts')
@endpush
