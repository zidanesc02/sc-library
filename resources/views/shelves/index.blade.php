@extends('layouts.app')

@section('content')
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Shelves</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Shelves</li>
                </ol>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">All Shelves</h4>
                    <div class="card-header-action">
                        <a href="{{ route('shelves.create') }}" class="btn btn-primary btn-sm">
                            <i class="mdi mdi-plus"></i> Create
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table mb-0" id="shelvesTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Code</th>
                                    <th>Location</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
            <!-- end card -->
        </div>
        <!-- end col -->
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            // DataTable
            $('#shelvesTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('shelves.json') }}",
                columns: [{
                        name: 'no',
                        data: 'id',
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                        orderable: false,
                    },
                    {
                        name: 'code',
                        data: 'code',
                        orderable: true
                    },
                    {
                        name: 'location',
                        data: 'location',
                        orderable: false
                    },
                    {
                        name: 'description',
                        data: 'description',
                        orderable: false,
                    },
                    {
                        name: 'action',
                        data: 'action',
                        orderable: false
                    },
                ]
            });

        });
    </script>
@endpush
