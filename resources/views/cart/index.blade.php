@extends('layouts.app')

@section('content')
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Cart</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active">Cart</li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-8">
            @foreach ($carts as $cart)
                <div class="card border shadow-none">
                    <div class="card-body">

                        <div class="d-flex align-items-start border-bottom pb-3">
                            <div class="me-4">
                                <img src="{{ env('APP_URL') . '/storage//' . ($cart->book_cover ? $cart->book_cover : 'no-cover.jpg') }}" alt="" class="avatar-lg">
                            </div>
                            <div class="flex-grow-1 align-self-center overflow-hidden">
                                <div>
                                    <h5 class="text-truncate font-size-16"><a href="#" class="text-dark">{{ $cart->book_title }}</a></h5>
                                    <p class="mb-1">Category : <span class="fw-medium">{{ $cart->category }}</span></p>
                                    <p>Shelf : <span class="fw-medium">{{ $cart->shelf }}</span></p>
                                </div>
                            </div>
                            <div class="flex-shrink-0 ms-2">
                                <ul class="list-inline mb-0 font-size-16">
                                    <li class="list-inline-item">
                                        <a href="{{ route('cart.remove', $cart->book_id) }}" class="text-muted px-1">
                                            <i class="mdi mdi-trash-can-outline"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mt-3">
                                        <p class="text-muted mb-2">Quantity</p>
                                        <input type="number" class="form-control" value="{{ $cart->quantity }}" name="quantity" onchange="updateQuantity(this.value, {{ $cart->id }})">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            @endforeach

            <div class="row my-4">
                <div class="col-sm-6">
                    <a href="{{ route('home') }}" class="btn btn-link text-muted">
                        <i class="mdi mdi-arrow-left me-1"></i> Continue Explore </a>
                </div> <!-- end col -->
                @if (count($carts) > 0)
                    <div class="col-sm-6">
                        <div class="text-sm-end mt-2 mt-sm-0">
                            <form action="{{ route('transactions.request-borrow') }}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-success">
                                    <i class="mdi mdi-cart-outline me-1"></i> Borrow the books </button>
                            </form>
                        </div>
                    </div>
                @endif
            </div> <!-- end row-->
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        function updateQuantity(value, cartId) {
            axios({
                method: 'post',
                url: "{{ route('cart.update-quantity') }}",
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                data: {
                    "_token": "{{ csrf_token() }}",
                    quantity: value,
                    cartId
                }
            }).then((response) => {
                Swal.fire({
                    position: 'top-right',
                    icon: 'success',
                    title: 'Successfully update quantity',
                    showConfirmButton: false,
                    timer: 1000
                })
            }).catch((err) => {
                Swal.fire({
                    position: 'top-right',
                    icon: 'error',
                    title: 'Oops...',
                    showConfirmButton: false,
                    timer: 1000,
                    text: err.response.data.message,
                })
            })
        }
    </script>
@endpush
