@extends('layouts.app')

@section('content')
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Dashboard</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="">Library</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-xl-4">
            <div class="card bg-primary">
                <div class="card-body">
                    <div class="text-center py-3">
                        <ul class="bg-bubbles ps-0">
                            <li><i class="bx bx-grid-alt font-size-24"></i></li>
                            <li><i class="bx bx-tachometer font-size-24"></i></li>
                            <li><i class="bx bx-store font-size-24"></i></li>
                            <li><i class="bx bx-cube font-size-24"></i></li>
                            <li><i class="bx bx-cylinder font-size-24"></i></li>
                            <li><i class="bx bx-command font-size-24"></i></li>
                            <li><i class="bx bx-hourglass font-size-24"></i></li>
                            <li><i class="bx bx-pie-chart-alt font-size-24"></i></li>
                            <li><i class="bx bx-coffee font-size-24"></i></li>
                            <li><i class="bx bx-polygon font-size-24"></i></li>
                        </ul>
                        <div class="main-wid position-relative">
                            <h3 class="text-white">Library Dashboard</h3>

                            <h3 class="text-white mb-0"> Welcome Back, {{ Auth::user()->name }}!</h3>

                            <div class="mt-4 pt-2 mb-2">
                                <a href="{{ route('home') }}" class="btn btn-success">Back to home <i class="mdi mdi-arrow-right ms-1"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-8">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="avatar">
                                <span class="avatar-title bg-soft-primary rounded">
                                    <i class="mdi mdi-shopping-outline text-primary font-size-24"></i>
                                </span>
                            </div>
                            <p class="text-muted mt-4 mb-0">Today Orders</p>
                            <h4 class="mt-1 mb-0">3,89,658 <sup class="text-success fw-medium font-size-14"><i class="mdi mdi-arrow-down"></i> 10%</sup></h4>
                            <div>
                                <div class="py-3 my-1">
                                    <div id="mini-1" data-colors='["#3980c0"]'></div>
                                </div>
                                <ul class="list-inline d-flex justify-content-between justify-content-center mb-0">
                                    <li class="list-inline-item"><a href="#" class="text-muted">Day</a></li>
                                    <li class="list-inline-item"><a href="#" class="text-muted">Week</a></li>
                                    <li class="list-inline-item"><a href="#" class="text-muted">Month</a></li>
                                    <li class="list-inline-item"><a href="#" class="text-muted">Year</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="avatar">
                                <span class="avatar-title bg-soft-success rounded">
                                    <i class="mdi mdi-eye-outline text-success font-size-24"></i>
                                </span>
                            </div>
                            <p class="text-muted mt-4 mb-0">Today Visitor</p>
                            <h4 class="mt-1 mb-0">1,648,29 <sup class="text-danger fw-medium font-size-14"><i class="mdi mdi-arrow-down"></i> 19%</sup></h4>
                            <div>
                                <div class="py-3 my-1">
                                    <div id="mini-2" data-colors='["#33a186"]'></div>
                                </div>
                                <ul class="list-inline d-flex justify-content-between justify-content-center mb-0">
                                    <li class="list-inline-item"><a href="#" class="text-muted">Day</a></li>
                                    <li class="list-inline-item"><a href="#" class="text-muted">Week</a></li>
                                    <li class="list-inline-item"><a href="#" class="text-muted">Month</a></li>
                                    <li class="list-inline-item"><a href="#" class="text-muted">Year</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="avatar">
                                <span class="avatar-title bg-soft-primary rounded">
                                    <i class="mdi mdi-rocket-outline text-primary font-size-24"></i>
                                </span>
                            </div>
                            <p class="text-muted mt-4 mb-0">Total Expense</p>
                            <h4 class="mt-1 mb-0">6,48,249 <sup class="text-success fw-medium font-size-14"><i class="mdi mdi-arrow-down"></i> 22%</sup></h4>
                            <div>
                                <div class="py-3 my-1">
                                    <div id="mini-3" data-colors='["#3980c0"]'></div>
                                </div>
                                <ul class="list-inline d-flex justify-content-between justify-content-center mb-0">
                                    <li class="list-inline-item"><a href="#" class="text-muted">Day</a></li>
                                    <li class="list-inline-item"><a href="#" class="text-muted">Week</a></li>
                                    <li class="list-inline-item"><a href="#" class="text-muted">Month</a></li>
                                    <li class="list-inline-item"><a href="#" class="text-muted">Year</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="avatar">
                                <span class="avatar-title bg-soft-success rounded">
                                    <i class="mdi mdi-account-multiple-outline text-success font-size-24"></i>
                                </span>
                            </div>
                            <p class="text-muted mt-4 mb-0">New Users</p>
                            <h4 class="mt-1 mb-0">$5,265,3 <sup class="text-danger fw-medium font-size-14"><i class="mdi mdi-arrow-down"></i> 18%</sup></h4>
                            <div>
                                <div class="py-3 my-1">
                                    <div id="mini-4" data-colors='["#33a186"]'></div>
                                </div>
                                <ul class="list-inline d-flex justify-content-between justify-content-center mb-0">
                                    <li class="list-inline-item"><a href="#" class="text-muted">Day</a></li>
                                    <li class="list-inline-item"><a href="#" class="text-muted">Week</a></li>
                                    <li class="list-inline-item"><a href="#" class="text-muted">Month</a></li>
                                    <li class="list-inline-item"><a href="#" class="text-muted">Year</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-lg-3 col-md-6 main-menu" onclick="location.href='{{ route('users.index') }}'" >
            <div class="card">
                <div class="card-body p-3 d-flex">
                    <div class="avatar mr-5">
                        <span class="avatar-title bg-soft-primary rounded">
                            <i class="mdi mdi-account text-primary font-size-24"></i>
                        </span>
                    </div>
                    <div class="flex-fill">
                        <p class="fw-bold font-size-20" style="margin-top: 0; margin-left: 20px; margin-bottom: 0">User</p>
                        <p class="font-size-14" style="margin-top: 0; margin-left: 20px; margin-bottom: 0">Manage User Data</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 main-menu" onclick="location.href='{{ route('shelves.index') }}'" >
            <div class="card">
                <div class="card-body p-3 d-flex">
                    <div class="avatar mr-5">
                        <span class="avatar-title bg-soft-primary rounded">
                            <i class="mdi mdi-bookshelf text-primary font-size-24"></i>
                        </span>
                    </div>
                    <div class="flex-fill">
                        <p class="fw-bold font-size-20" style="margin-top: 0; margin-left: 20px; margin-bottom: 0">Shelf</p>
                        <p class="font-size-14" style="margin-top: 0; margin-left: 20px; margin-bottom: 0">Manage Location Book</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 main-menu" onclick="location.href='{{ route('books.index') }}'" >
            <div class="card">
                <div class="card-body p-3 d-flex">
                    <div class="avatar mr-5">
                        <span class="avatar-title bg-soft-primary rounded">
                            <i class="mdi mdi-book-open text-primary font-size-24"></i>
                        </span>
                    </div>
                    <div class="flex-fill">
                        <p class="fw-bold font-size-20" style="margin-top: 0; margin-left: 20px; margin-bottom: 0">Book</p>
                        <p class="font-size-14" style="margin-top: 0; margin-left: 20px; margin-bottom: 0">Manage Book</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 main-menu" onclick="location.href='{{ route('transactions.index') }}'" >
            <div class="card">
                <div class="card-body p-3 d-flex">
                    <div class="avatar mr-5">
                        <span class="avatar-title bg-soft-primary rounded">
                            <i class="mdi mdi-swap-horizontal text-primary font-size-24"></i>
                        </span>
                    </div>
                    <div class="flex-fill">
                        <p class="fw-bold font-size-20" style="margin-top: 0; margin-left: 20px; margin-bottom: 0">Transaction</p>
                        <p class="font-size-14" style="margin-top: 0; margin-left: 20px; margin-bottom: 0">Borrow and return</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
