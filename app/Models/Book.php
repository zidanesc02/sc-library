<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'isbn',
        'category_id',
        'shelf_id',
        'title',
        'published_year',
        'stock',
        'author',
        'publisher',
        'page_count',
        'synopsis',
        'cover'
    ];
}
