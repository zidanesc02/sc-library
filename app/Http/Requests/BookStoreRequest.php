<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'isbn' => 'required|unique:books,id,:id|max:255',
            'category' => 'required|exists:categories,id',
            'shelf' => 'required|exists:shelves,id',
            'title' => 'required|max:255',
            'published_year' => 'required|integer',
            'stock' => 'required|integer',
            'author' => 'required|max:255',
            'publisher' => 'required|max:255',
            'page_count' => 'required|integer',
            'synopsis' => 'required|max:1000',
            'cover' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'cover_default' => 'nullable',
        ];
    }
}
