<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,id,:id'],
            'new_password' => ['nullable', 'string', 'min:8'],
            'gender' => ['required', 'in:male,female'],
            'phone_number' => ['required', 'numeric'],
            'address' => ['required', 'min:3', 'max:80'],
            'avatar' => ['nullable', 'image', 'mimes:jpeg,png,jpg'],
            'role' => ['required', 'in:admin,member'],
            'avatar_default' => ['nullable'],
        ];
    }
}
