<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['pageTitle'] = "Home";
        $data['categories'] = DB::table('categories')->get();
        $data['shelves'] = DB::table('shelves')->get();
        $data['books'] = DB::table('books')
            ->leftJoin('categories', 'books.category_id', '=', 'categories.id')
            ->leftJoin('shelves', 'books.shelf_id', '=', 'shelves.id')
            ->get([
                'books.*',
                'categories.name as category',
                'shelves.code as shelf'
            ]);

        return view('home', $data);
    }
}
