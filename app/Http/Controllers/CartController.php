<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    public function index()
    {
        $data['pageTitle'] = "Cart";
        $data['carts'] = DB::table('carts')
            ->leftJoin('books', 'books.id', '=', 'carts.book_id')
            ->leftJoin('shelves', 'shelves.id', '=', 'books.shelf_id')
            ->leftJoin('categories', 'categories.id', '=', 'books.category_id')
            ->where('carts.user_id', auth()->user()->id)
            ->get([
                'carts.*',
                'books.title as book_title',
                'books.cover as book_cover',
                'books.id as book_id',
                'shelves.code as shelf',
                'categories.name as category'
            ]);

        return view('cart.index', $data);
    }

    public function addToCart($bookId)
    {
        $book = DB::table('books')->where('id', $bookId)->first();

        $isItemExist = DB::table('carts')
            ->where('user_id', auth()->user()->id)
            ->where('book_id', $bookId)
            ->first();

        if ($isItemExist) {
            if ($isItemExist->quantity + 1 > $book->stock) {
                return redirect()->back()->with('failed', 'Stock is not enough.');
            }

            DB::table('carts')
                ->where('user_id', auth()->user()->id)
                ->where('book_id', $bookId)
                ->increment('quantity');
        } else {
            if ($book->stock < 1) {
                return redirect()->back()->with('failed', 'Stock is not enough.');
            }

            DB::table('carts')->insert([
                'user_id' => auth()->user()->id,
                'book_id' => $bookId,
                'quantity' => 1,
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }

        return redirect()->back()->with('success', 'Book added to cart successfully!');
    }

    public function removeFromCart($bookId)
    {
        DB::table('carts')
            ->where('user_id', auth()->user()->id)
            ->where('book_id', $bookId)
            ->delete();

        return redirect()->back()->with('success', 'Book removed from cart successfully!');
    }

    public function changeQuantity(Request $request)
    {
        $cartId = $request->cartId;
        $quantity = intval($request->quantity);

        $cart = DB::table('carts')
            ->where('id', $cartId)
            ->first();

        $book = DB::table('books')->where('id', $cart->book_id)->first();

        if ($quantity > $book->stock) {
            return response()->json(['message' => 'Stock is not enough.'], 400);
        }

        if ($quantity < 1) {
            return response()->json(['message' => 'Quantity must be greater than 0.'], 400);
        }

        DB::table('carts')
            ->where('id', $cartId)
            ->update([
                'quantity' => $quantity
            ]);

        return response()->json(['message' => 'Quantity changed successfully.'], 200);
    }
}
