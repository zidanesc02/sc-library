<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShelfeStoreRequest;
use App\Models\Shelf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShelfController extends Controller
{
    public function index()
    {
        $data['pageTitle'] = 'Shelves';

        return view('shelves.index', $data);
    }

    public function create()
    {
        $data['pageTitle'] = 'Create Shelf';
        $data['code'] = $this->generateCode();

        return view('shelves.create', $data);
    }

    public function store(ShelfeStoreRequest $request)
    {
        DB::table('shelves')->insert([
            'code' => $request->code,
            'location' => $request->location,
            'description' => $request->description,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return redirect()->route('shelves.index')->with('success', 'Successfully created a new shelf!');
    }

    public function edit($id)
    {
        $data['pageTitle'] = 'Edit Shelf';
        $data['shelf'] = DB::table('shelves')->where('id', $id)->first();

        return view('shelves.edit', $data);
    }

    public function update(ShelfeStoreRequest $request, $id)
    {
        DB::table('shelves')->where('id', $id)->update([
            'location' => $request->location,
            'description' => $request->description,
            // 'updated_at' => now()
        ]);

        return redirect()->route('shelves.index')->with('success', 'Shelf updated successfully.');
    }

    public function destroy($id)
    {
        DB::table('shelves')->where('id', $id)->delete();

        return response()->json(['message' => 'Shelf deleted successfully.']);
    }

    private function generateCode($number = 0)
    {
        $lastCode = DB::table('shelves')->orderBy('code', 'desc')->first();
        $code = 'RAK-001';

        if ($lastCode) {
            $lastCode = $lastCode->code;
            $lastCode = explode('-', $lastCode);
            $lastCode = $lastCode[1];
            $lastCode = intval($lastCode) + $number + 1;
            $lastCode = str_pad($lastCode, 3, '0', STR_PAD_LEFT);
            $code = 'RAK-' . $lastCode;

            $isCodeExist = DB::table('shelves')->where('code', $code)->exists();
            if ($isCodeExist) {
                $number++;
                $code = $this->generateCode(++$number);
            }
        }

        return $code;
    }

    /* Method for datatable that returning json */
    public function getShelvesJson(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start"); // Start counting from this
        $rowPerPage = $request->get("length"); // total number of rows per page

        $columnIndexArr = $request->get('order');
        $columnNameArr = $request->get('columns');
        $orderArr = $request->get('order');
        $searchArr = $request->get('search');

        $columnIndex = $columnIndexArr[0]['column']; // Column index
        $columnName = $columnNameArr[$columnIndex]['data']; // Column name
        $columnSortOrder = $orderArr[0]['dir']; // asc or desc
        $searchValue = $searchArr['value']; // Search value

        // Total records
        $totalRecords = DB::table('shelves')->select('count(*) as allcount')->count();
        $totalRecordswithFilter = DB::table('shelves')->select('count(*) as allcount')->where('code', 'like', '%' . $searchValue . '%')->count();

        // Get records, also we have included search filter as well
        $records = DB::table('shelves')->orderBy($columnName, $columnSortOrder)
            ->where('shelves.code', 'like', '%' . $searchValue . '%')
            ->orWhere('shelves.location', 'like', '%' . $searchValue . '%')
            ->select('shelves.*')
            ->skip($start)
            ->take($rowPerPage)
            ->get();

        $data = [];

        $actionButton = '<button type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                            Action <i class="mdi mdi-chevron-down"></i>
                        </button>';

        foreach ($records as $record) {
            array_push($data, [
                "id" => $record->id,
                "code" => $record->code,
                "location" => $record->location,
                "description" => $record->descrption ?? 'There is no description.',
                "action" => $actionButton . ' <ul class="dropdown-menu">
                                                    <li><a class="dropdown-item" href="' . route('shelves.edit', $record->id) . '">Edit</a></li>
                                                    <li><a
                                                        class="dropdown-item"
                                                        href="#"
                                                        onClick="modalDelete(\'Shelf\', \''. $record->code .'\', \''. route('shelves.destroy', $record->id) .'\', \''. route('shelves.index') .'\')">
                                                            Delete
                                                    </a></li>
                                                </ul>',
            ]);
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data,
        );

        echo json_encode($response);
    }
}
