<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookStoreRequest;
use App\Http\Requests\StockRequest;
use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BookController extends Controller
{
    public function index()
    {
        $data['pageTitle'] = 'Books';

        return view('books.index', $data);
    }

    public function create()
    {
        $data['pageTitle'] = 'Add Book';
        $data['categories'] = DB::table('categories')->get();
        $data['shelves'] = DB::table('shelves')->get();

        return view('books.create', $data);
    }

    public function store(BookStoreRequest $request)
    {
        $imageName = null;

        if ($request->hasFile('cover')) {
            $imageName = time() . '.' . $request->cover->extension();
            $request->cover->storeAs('public', $imageName);
        }

        DB::table('books')->insert([
            'isbn' => $request->isbn,
            'category_id' => $request->category,
            'shelf_id' => $request->shelf,
            'title' => $request->title,
            'published_year' => $request->published_year,
            'stock' => $request->stock,
            'author' => $request->author,
            'publisher' => $request->publisher,
            'page_count' => $request->page_count,
            'synopsis' => $request->synopsis,
            'cover' => $imageName,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return redirect()->route('books.index')->with('success', 'Book created successfully.');
    }

    public function show($id)
    {
        $data['pageTitle'] = 'Book Details';
        $data['book'] = DB::table('books')
        ->where('books.id', $id)
        ->leftJoin('shelves', 'shelves.id', '=', 'books.shelf_id')
        ->leftJoin('categories', 'categories.id', '=', 'books.category_id')
        ->first([
                'books.*',
                'shelves.code as shelf',
                'categories.name as category'
            ]);

        return view('books.show', $data);
    }

    public function edit($id)
    {
        $data['pageTitle'] = 'Edit Book';
        $data['categories'] = DB::table('categories')->get();
        $data['shelves'] = DB::table('shelves')->get();
        $data['book'] = DB::table('books')->where('id', $id)->first();

        return view('books.edit', $data);
    }

    public function update(BookStoreRequest $request, $id)
    {
        $book = DB::table('books')->where('id', $id)->first();
        $imageName = $book->cover;

        if ($request->cover_default || $request->hasFile('cover')) {
            if (Storage::exists('public/' . $book->cover)) {
                Storage::delete('public/' . $book->cover);
                $imageName = null;
            }
        }

        if ($request->hasFile('cover')) {
            $imageName = time() . '.' . $request->cover->extension();
            $request->cover->storeAs('public', $imageName);
        }

        DB::table('books')->where('id', $id)->update([
            'isbn' => $request->isbn,
            'category_id' => $request->category,
            'shelf_id' => $request->shelf,
            'title' => $request->title,
            'published_year' => $request->published_year,
            'author' => $request->author,
            'publisher' => $request->publisher,
            'page_count' => $request->page_count,
            'synopsis' => $request->synopsis,
            'cover' => $imageName,
            'updated_at' => now()
        ]);

        return redirect()->route('books.index')->with('success', 'Book updated successfully.');
    }

    public function destroy($id)
    {
        $book = DB::table('books')->where('id', $id)->first();
        if (Storage::exists('public/' . $book->cover)) {
            Storage::delete('public/' . $book->cover);
        }

        DB::table('books')->where('id', $id)->delete();

        return response()->json(['message' => 'Book deleted successfully.']);
    }

    public function addStock(StockRequest $request, $id)
    {
        DB::table('books')->where('id', $id)->increment('stock', $request->stock);
        DB::table('history_stock_book')->insert([
            'book_id' => $id,
            'stock' => $request->stock,
            'description' => $request->description,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return response()->json(['message' => 'Add stock book success.']);
    }

    /* Method for datatable that returning json */
    public function getBooksJson(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start"); // Start counting from this
        $rowPerPage = $request->get("length"); // total number of rows per page

        $columnIndexArr = $request->get('order');
        $columnNameArr = $request->get('columns');
        $orderArr = $request->get('order');
        $searchArr = $request->get('search');

        $columnIndex = $columnIndexArr[0]['column']; // Column index
        $columnName = $columnNameArr[$columnIndex]['data']; // Column name
        $columnSortOrder = $orderArr[0]['dir']; // asc or desc
        $searchValue = $searchArr['value']; // Search value

        // Total records
        $totalRecords = DB::table('books')->select('count(*) as allcount')->count();
        $totalRecordswithFilter = DB::table('books')->select('count(*) as allcount')->where('title', 'like', '%' . $searchValue . '%')->count();

        // Get records, also we have included search filter as well
        $records = DB::table('books')->orderBy($columnName, $columnSortOrder)
            ->where('books.title', 'like', '%' . $searchValue . '%')
            ->orWhere('books.published_year', 'like', '%' . $searchValue . '%')
            ->orWhere('books.author', 'like', '%' . $searchValue . '%')
            ->orWhere('books.publisher', 'like', '%' . $searchValue . '%')
            ->leftJoin('categories', 'books.category_id', '=', 'categories.id')
            ->leftJoin('shelves', 'books.shelf_id', '=', 'shelves.id')
            ->select([
                'books.*',
                'categories.name as category_name',
                'shelves.code as shelf_code',
            ])
            ->skip($start)
            ->take($rowPerPage)
            ->get();

        $data = [];

        $actionButton = '<button type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                            Action <i class="mdi mdi-chevron-down"></i>
                        </button>';

        foreach ($records as $record) {
            $coverUrl = env('APP_URL') . '/storage//' . ($record->cover ? $record->cover : 'no-cover.jpg');

            array_push($data, [
                "id" => $record->id,
                "isbn" => $record->isbn,
                "cover" => '<img src="' . $coverUrl . '" alt="" class="avatar-lg">',
                "title" => $record->title,
                "stock" => $record->stock . '<button type="button" class="btn btn-sm btn-success waves-effect waves-light" onClick="addStock(' . $record->id  . ', \'' . $record->title . '\')">+</button>',
                "category" => $record->category_name ?? '-',
                "shelf" => $record->shelf_code ?? '-',
                "action" => $actionButton . ' <ul class="dropdown-menu">
                                                    <li><a class="dropdown-item" href="' . route('books.edit', $record->id) . '">Edit</a></li>
                                                    <li><a
                                                        class="dropdown-item"
                                                        href="#"
                                                        onClick="modalDelete(\'Book\', \'' . $record->title . '\', \'' . route('books.destroy', $record->id) . '\', \'' . route('books.index') . '\')">
                                                            Delete
                                                    </a></li>
                                                </ul>',
            ]);
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data,
        );

        echo json_encode($response);
    }
}
