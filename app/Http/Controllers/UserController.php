<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function index()
    {
        $data['pageTitle'] = 'Users';

        return view('users.index', $data);
    }

    public function create()
    {
        return view('users.create');
    }

    public function store(UserStoreRequest $request)
    {
        $imageName = null;

        if ($request->hasFile('avatar')) {
            $imageName = time() . '.' . $request->avatar->extension();
            $request->avatar->storeAs('public', $imageName);
        }

        DB::table('users')->insert([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'gender' => $request->gender,
            'status' => true,
            'phone_number' => $request->phone_number,
            'address' => $request->address,
            'role' => $request->role,
            'avatar' => $imageName,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return redirect()->route('users.index')->with('success', 'User created successfully.');
    }

    public function show($id)
    {
        $data['pageTitle'] = 'User Details';
        $data['user'] = DB::table('users')->where('id', $id)->first();

        return view('users.show', $data);
    }

    public function edit($id)
    {
        $data['pageTitle'] = 'Edit User';
        $data['user'] = DB::table('users')->where('id', $id)->first();

        return view('users.edit', $data);
    }

    public function update(UserUpdateRequest $request, $id)
    {
        $user = DB::table('users')->where('id', $id)->first();
        $imageName = $user->avatar;


        if ($request->avatar_default || $request->hasFile('avatar')) {
            $imageName = null;
            if (Storage::exists('public/' . $user->avatar)) {
                Storage::delete('public/' . $user->avatar);
            }
        }

        if ($request->hasFile('avatar')) {
            $imageName = time() . '.' . $request->avatar->extension();
            $request->avatar->storeAs('public', $imageName);
        }

        DB::table('users')->where('id', $id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'gender' => $request->gender,
            'phone_number' => $request->phone_number,
            'address' => $request->address,
            'role' => $request->role,
            'avatar' => $imageName,
            'updated_at' => now()
        ]);

        return redirect()->route('users.index')->with('success', 'User updated successfully.');;
    }

    public function destroy($id)
    {
        $user = DB::table('users')->where('id', $id)->first();
        if (Storage::exists('public/' . $user->avatar)) {
            Storage::delete('public/' . $user->avatar);
        }

        DB::table('users')->where('id', $id)->delete();

        return response()->json(['message' => 'User deleted successfully.']);
    }

    public function changeStatus($id, $status)
    {
        DB::table('users')->where('id', $id)->update([
            'status' => $status == 'false' ? 0 : 1,
            'updated_at' => now()
        ]);

        return response()->json(['message' => 'User status changed successfully.']);
    }

    /* Method for datatable that returning json */
    public function getRequestJson(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start"); // Start counting from this
        $rowPerPage = $request->get("length"); // total number of rows per page

        $columnIndexArr = $request->get('order');
        $columnNameArr = $request->get('columns');
        $orderArr = $request->get('order');
        $searchArr = $request->get('search');

        $columnIndex = $columnIndexArr[0]['column']; // Column index
        $columnName = $columnNameArr[$columnIndex]['data']; // Column name
        $columnSortOrder = $orderArr[0]['dir']; // asc or desc
        $searchValue = $searchArr['value']; // Search value

        // Total records
        $totalRecords = DB::table('users')->where('status', false)->where('role', 'member')->select('count(*) as allcount')->count();
        $totalRecordswithFilter = DB::table('users')->where('status', false)->where('role', 'member')->select('count(*) as allcount')->where('name', 'like', '%' . $searchValue . '%')->count();

        // Get records, also we have included search filter as well
        $records = DB::table('users')->orderBy($columnName, $columnSortOrder)
            ->where('role', 'member')
            ->where('status', false)
            ->where(function ($q) use ($searchValue) {
                $q->where('users.name', 'like', '%' . $searchValue . '%')
                    ->orWhere('users.email', 'like', '%' . $searchValue . '%');
            })
            ->select([
                'users.*',
            ])
            ->skip($start)
            ->take($rowPerPage)
            ->get();

        $data = [];

        foreach ($records as $record) {
            $avatarUrl = env('APP_URL') . '/storage//' . ($record->avatar ? $record->avatar : 'no-cover.jpg');

            array_push($data, [
                "id" => $record->id,
                "name" => '<img src="' . $avatarUrl . '" alt="" class="avatar-sm rounded-circle me-2">
                            <a href="#" class="text-body">' . $record->name . '</a>',
                "email" => $record->email,
                "status" => '<input type="checkbox" id="' . $record->id . '" switch="success" ' . ($record->status ? 'checked' : '') . '  onChange="changeStatus(this.checked, ' . $record->id . ')"/>
                            <label for="' . $record->id . '" data-on-label="Yes" data-off-label="No"></label>',
                "action" => '<ul class="list-inline mb-0">
                                <li class="list-inline-item">
                                    <a href="' . route('users.edit', $record->id) . '" class="px-2 text-primary"><i class="bx bx-pencil font-size-18"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#" class="px-2 text-danger" onClick="modalDelete(\'Member\', \'' . $record->name . '\', \'' . route('users.destroy', $record->id) . '\', \'' . route('users.index') . '\')"><i class="bx bx-trash-alt font-size-18"></i></a>
                                </li>
                            </ul>',
            ]);
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data,
        );

        echo json_encode($response);
    }

    public function getMemberJson(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start"); // Start counting from this
        $rowPerPage = $request->get("length"); // total number of rows per page

        $columnIndexArr = $request->get('order');
        $columnNameArr = $request->get('columns');
        $orderArr = $request->get('order');
        $searchArr = $request->get('search');

        $columnIndex = $columnIndexArr[0]['column']; // Column index
        $columnName = $columnNameArr[$columnIndex]['data']; // Column name
        $columnSortOrder = $orderArr[0]['dir']; // asc or desc
        $searchValue = $searchArr['value']; // Search value

        // Total records
        $totalRecords = DB::table('users')->where('status', true)->where('role', 'member')->select('count(*) as allcount')->count();
        $totalRecordswithFilter = DB::table('users')->where('status', true)->where('role', 'member')->select('count(*) as allcount')->where('name', 'like', '%' . $searchValue . '%')->count();

        // Get records, also we have included search filter as well
        $records = DB::table('users')->orderBy($columnName, $columnSortOrder)
            ->where('role', 'member')
            ->where('status', true)
            ->where(function ($q) use ($searchValue) {
                $q->where('users.name', 'like', '%' . $searchValue . '%')
                    ->orWhere('users.email', 'like', '%' . $searchValue . '%');
            })
            ->select([
                'users.*',
            ])
            ->skip($start)
            ->take($rowPerPage)
            ->get();

        $data = [];

        foreach ($records as $record) {
            $avatarUrl = env('APP_URL') . '/storage//' . ($record->avatar ? $record->avatar : 'no-cover.jpg');

            array_push($data, [
                "id" => $record->id,
                "name" => '<img src="' . $avatarUrl . '" alt="" class="avatar-sm rounded-circle me-2">
                            <a href="#" class="text-body">' . $record->name . '</a>',
                "email" => $record->email,
                "status" => '<input type="checkbox" id="' . $record->id . '" switch="success" ' . ($record->status ? 'checked' : '') . '  onChange="changeStatus(this.checked, ' . $record->id . ')"/>
                            <label for="' . $record->id . '" data-on-label="Yes" data-off-label="No"></label>',
                "action" => '<ul class="list-inline mb-0">
                                <li class="list-inline-item">
                                    <a href="' . route('users.edit', $record->id) . '" class="px-2 text-primary"><i class="bx bx-pencil font-size-18"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#" class="px-2 text-danger" onClick="modalDelete(\'Member\', \'' . $record->name . '\', \'' . route('users.destroy', $record->id) . '\', \'' . route('users.index') . '\')"><i class="bx bx-trash-alt font-size-18"></i></a>
                                </li>
                            </ul>',
            ]);
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data,
        );

        echo json_encode($response);
    }

    public function getAdminJson(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start"); // Start counting from this
        $rowPerPage = $request->get("length"); // total number of rows per page

        $columnIndexArr = $request->get('order');
        $columnNameArr = $request->get('columns');
        $orderArr = $request->get('order');
        $searchArr = $request->get('search');

        $columnIndex = $columnIndexArr[0]['column']; // Column index
        $columnName = $columnNameArr[$columnIndex]['data']; // Column name
        $columnSortOrder = $orderArr[0]['dir']; // asc or desc
        $searchValue = $searchArr['value']; // Search value

        // Total records
        $totalRecords = DB::table('users')->where('role', 'admin')->select('count(*) as allcount')->count();
        $totalRecordswithFilter = DB::table('users')->where('role', 'admin')->select('count(*) as allcount')->where('name', 'like', '%' . $searchValue . '%')->count();

        // Get records, also we have included search filter as well
        $records = DB::table('users')->orderBy($columnName, $columnSortOrder)
            ->where('role', 'admin')
            ->where(function ($q) use ($searchValue) {
                $q->where('users.name', 'like', '%' . $searchValue . '%')
                    ->orWhere('users.email', 'like', '%' . $searchValue . '%');
            })
            ->select([
                'users.*',
            ])
            ->skip($start)
            ->take($rowPerPage)
            ->get();

        $data = [];

        foreach ($records as $record) {
            $avatarUrl = env('APP_URL') . '/storage//' . ($record->avatar ? $record->avatar : 'no-cover.jpg');

            array_push($data, [
                "id" => $record->id,
                "name" => '<img src="' . $avatarUrl . '" alt="" class="avatar-sm rounded-circle me-2">
                            <a href="#" class="text-body">' . $record->name . '</a>',
                "email" => $record->email,
                "status" => '<input type="checkbox" id="' . $record->id . '" switch="success" ' . ($record->status ? 'checked' : '') . '  onChange="changeStatus(this.checked, ' . $record->id . ')"/>
                            <label for="' . $record->id . '" data-on-label="Yes" data-off-label="No"></label>',
                "action" => '<ul class="list-inline mb-0">
                                <li class="list-inline-item">
                                    <a href="' . route('users.edit', $record->id) . '" class="px-2 text-primary"><i class="bx bx-pencil font-size-18"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#" class="px-2 text-danger" onClick="modalDelete(\'Member\', \'' . $record->name . '\', \'' . route('users.destroy', $record->id) . '\', \'' . route('users.index') . '\')"><i class="bx bx-trash-alt font-size-18"></i></a>
                                </li>
                            </ul>',
            ]);
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data,
        );

        echo json_encode($response);
    }
}
