<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    public function requestBorrow()
    {
        $member = DB::table('users')
        ->where('id', auth()->user()->id)
        ->first();

        if (!$member->status) {
            return redirect()->back()->with('failed', 'Failed, Your account haven\'t been activated yet');
        }

        $transaction = DB::table('transactions')->insertGetId([
            'member_id' => auth()->user()->id,
            'status' => 'requested',
            'created_at' => now(),
            'updated_at' => now()
        ]);


        $carts = DB::table('carts')
            ->where('user_id', auth()->user()->id)
            ->get();

        foreach ($carts as $cart) {
            DB::table('transaction_details')->insert([
                'transaction_id' => $transaction,
                'book_id' => $cart->book_id,
                'quantity' => $cart->quantity,
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }

        DB::table('carts')
            ->where('user_id', auth()->user()->id)
            ->delete();

        return redirect()->back()->with('success', 'Requested borrow successfully');
    }

    public function index()
    {
        $data['pageTitle'] = 'Transactions';

        return view('transactions.index', $data);
    }

    public function changeStatus($id, $status)
    {
        switch ($status) {
            case 'accepted':
                DB::table('transactions')
                    ->where('id', $id)
                    ->update([
                        'status' => 'accepted',
                        'accepted_at' => now(),
                        'admin_id' => auth()->user()->id
                    ]);
                break;
            case 'declined':
                DB::table('transactions')
                    ->where('id', $id)
                    ->update([
                        'status' => 'declined',
                        'admin_id' => auth()->user()->id
                    ]);
                break;
            case 'completed':
                DB::table('transactions')
                    ->where('id', $id)
                    ->update([
                        'status' => 'completed',
                        'completed_at' => now(),
                        'admin_id' => auth()->user()->id
                    ]);
                break;
            default:
                return response()->json(['error' => 'Invalid status'], 400);
                break;
        }

        return response()->json(['success' => 'Status changed successfully'], 200);
    }

    /* Method for datatable that returning json */
    public function getTransactionJson(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start"); // Start counting from this
        $rowPerPage = $request->get("length"); // total number of rows per page

        $columnIndexArr = $request->get('order');
        $columnNameArr = $request->get('columns');
        $orderArr = $request->get('order');
        $searchArr = $request->get('search');

        $columnIndex = $columnIndexArr[0]['column']; // Column index
        $columnName = $columnNameArr[$columnIndex]['data']; // Column name
        $columnSortOrder = $orderArr[0]['dir']; // asc or desc
        $searchValue = $searchArr['value']; // Search value

        // Total records
        $totalRecords = DB::table('transactions')->select('count(*) as allcount')->count();
        $totalRecordswithFilter = DB::table('transactions')->select('count(*) as allcount')->where('created_at', 'like', '%' . $searchValue . '%')->count();

        // Get records, also we have included search filter as well
        $records = DB::table('transactions')->orderBy($columnName, $columnSortOrder)
            ->where(function ($q) use ($searchValue) {
                $q->where('transactions.created_at', 'like', '%' . $searchValue . '%');
            })
            ->leftJoin('users', 'users.id', '=', 'transactions.member_id')
            ->select([
                'transactions.*',
                'users.name as member_name',
                'users.email as member_email',
            ])
            ->skip($start)
            ->take($rowPerPage)
            ->get();

        $data = [];

        foreach ($records as $record) {
            $books = "";
            $transactionDetails = DB::table('transaction_details')
                ->where('transaction_id', $record->id)
                ->leftJoin('books', 'books.id', '=', 'transaction_details.book_id')
                ->get([
                    'books.title as book_name',
                    'transaction_details.quantity'
                ]);

            foreach ($transactionDetails as $transactionDetail) {
                $books .= $transactionDetail->book_name . '(' . $transactionDetail->quantity . '), ';
            }

            array_push($data, [
                "id" => $record->id,
                "member_name" => $record->member_name,
                "member_email" => $record->member_email,
                "requested_at" => $record->created_at,
                "books" => substr($books, 0, -2),
                "status" => '<select class="form-control" name="status" onChange="changeStatus(this.value, ' . $record->id . ')"/>
                                    <option value="requested" ' . ($record->status == 'requested' ? 'selected' : '')  . '>Requested</option>
                                    <option value="accepted" ' . ($record->status == 'accepted' ? 'selected' : '')  . '>Accepted</option>
                                    <option value="declined" ' . ($record->status == 'declined' ? 'selected' : '')  . '>Declined</option>
                                    <option value="completed" ' . ($record->status == 'completed' ? 'selected' : '')  . '>Completed</option>
                            </select>',
            ]);
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data,
        );

        echo json_encode($response);
    }
}
