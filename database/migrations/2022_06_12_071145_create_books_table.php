<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('isbn')->unique();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('shelf_id')->nullable();
            $table->string('title');
            $table->integer('published_year');
            $table->integer('stock')->default(0);
            $table->string('author');
            $table->string('publisher');
            $table->integer('page_count');
            $table->text('synopsis')->fulltext();
            $table->foreign('category_id')->references('id')->on('categories')->nullOnDelete();
            $table->foreign('shelf_id')->references('id')->on('shelves')->nullOnDelete();
            $table->text('cover')->nullable();

            // indexes
            $table->index(['title', 'published_year', 'author', 'publisher'], 'books_index');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
