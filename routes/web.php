<?php

use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// book detail
Route::get('/book/{id}', [App\Http\Controllers\BookController::class, 'show'])->name('book.show');

Route::middleware(['auth'])->group(function() {

    // Transactions
    Route::post('/transactions/request-borrow', [App\Http\Controllers\TransactionController::class, 'requestBorrow'])->name('transactions.request-borrow');


    Route::get('/cart', [App\Http\Controllers\CartController::class, 'index'])->name('cart.index');
    Route::get('/cart/add/{bookId}', [App\Http\Controllers\CartController::class, 'addToCart'])->name('cart.add');
    Route::get('/cart/remove/{bookId}', [App\Http\Controllers\CartController::class, 'removeFromCart'])->name('cart.remove');
    Route::post('/cart/update-quantity', [App\Http\Controllers\CartController::class, 'changeQuantity'])->name('cart.update-quantity');

    Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function() {
        Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');

        // Shelf Routes
        Route::get('shelves/json', [App\Http\Controllers\ShelfController::class, 'getShelvesJson'])->name('shelves.json');
        Route::resource('shelves', App\Http\Controllers\ShelfController::class)->except('show');

        // Book Routes
        Route::get('books/json', [App\Http\Controllers\BookController::class, 'getBooksJson'])->name('books.json');
        Route::resource('books', App\Http\Controllers\BookController::class)->except('show');

        // User Routes
        Route::post('users/change-status/{id}/{status}', [App\Http\Controllers\UserController::class, 'changeStatus'])->name('users.change-status');
        Route::get('users/request/json', [App\Http\Controllers\UserController::class, 'getRequestJson'])->name('users.request.json');
        Route::get('users/admin/json', [App\Http\Controllers\UserController::class, 'getAdminJson'])->name('users.admin.json');
        Route::get('users/member/json', [App\Http\Controllers\UserController::class, 'getMemberJson'])->name('users.member.json');
        Route::resource('users', App\Http\Controllers\UserController::class);

        // Transaction Routes
        Route::post('transactions/change-status/{id}/{status}', [App\Http\Controllers\TransactionController::class, 'changeStatus'])->name('transactions.change-status');
        Route::get('transactions/json', [App\Http\Controllers\TransactionController::class, 'getTransactionJson'])->name('transactions.json');
        Route::get('transactions', [App\Http\Controllers\TransactionController::class, 'index'])->name('transactions.index');
    });
});

